package com.apirest.back02.productosmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductosmongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductosmongodbApplication.class, args);
	}

}
