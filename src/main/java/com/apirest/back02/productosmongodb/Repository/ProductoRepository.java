package com.apirest.back02.productosmongodb.Repository;

import com.apirest.back02.productosmongodb.Models.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> { }
