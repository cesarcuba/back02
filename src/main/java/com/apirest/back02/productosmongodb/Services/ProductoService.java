package com.apirest.back02.productosmongodb.Services;

import com.apirest.back02.productosmongodb.Models.ProductoModel;
import com.apirest.back02.productosmongodb.Repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String  id) {
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel entity) {
        return productoRepository.save(entity);
    }

    public boolean delete(ProductoModel entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}
